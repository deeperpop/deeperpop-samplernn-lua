#!/usr/bin/env sh
# Perform dataset preparation
lua -l set_paths ./SampleRNN_torch/scripts/generate_dataset.lua \
  -source_path /farmshare/user_data/connorb3/deeperpop/data/wav/popular85/ \
  -dest_path /farmshare/user_data/connorb3/deeperpop/data/wav/popular85-srnn/

# Add symlink to dataset
ln -s /farmshare/user_data/connorb3/user_data/connorb3/deeperpop/data/wav/popular85-srnn \
  ./SampleRNN_torch/datasets/popular85
