#!/usr/bin/env sh
# Install dependencies
luarocks install --tree=lua_modules nn
luarocks install --tree=lua_modules cunn
luarocks install --tree=lua_modules rnn
luarocks install --tree=lua_modules optim
luarocks install --tree=lua_modules audio
luarocks install --tree=lua_modules xlua
luarocks install --tree=lua_modules gnuplot
